= waf-slides

"How we manage large numbers of ModSecurity WAF deployments, including CRS and custom rules"

Presented by: Peter Bittner, DevOps engineer at VSHN AG, Zurich

Available from: https://vshn.gitlab.io/waf-slides/

== About VSHN AG

VSHN automates the operation of applications in the cloud or on-premise,
so that software developers can focus on their business. VSHN supports
software developers in making applications automatically testable,
deployable and scalable and operating them on any infrastructure. In
addition to close and agile cooperation and consulting, we also take
over responsibility for the stability of our services, including 24/7
support.

== About Peter

I am a developer - of people, companies and code. By day, I work for
VSHN as a DevOps engineer, supporting our customers with getting their
applications working smoothly on our cloud platforms and getting up to
speed with modern development best-practices. By night, I maintain a
handful of free and open source projects, usually related to improving
software development workflows, test-driven development and deployment
automation. I'm in love with a snake. Her name is Python.

== About the talk

As a Swiss company, we have customers that care deeply about application
security, typically because they operate in the financial, insurance or
legal sector. We manage different WAF brands for them, ModSecurity being
the only 100% free and open source solution among those. To make
deployments, updating of the WAF engine and the deployed CRS, as well as
tuning their custom rules manageable, we need to do a great deal of
automation. This talk explains how we can serve several customers that
operate tens to hundreds of WAFs, requiring their developers and
development agencies to protect all public-facing applications by a Web
Application Firewall.
