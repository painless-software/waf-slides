:author: Peter Bittner, VSHN AG
:doctitle: Managing 100+ WAFs
:email: support@vshn.ch
:producer: VSHN AG
:creator: VSHN AG
:revnumber: 1.0
:revdate: {docdate}
:copyright: CC-BY-SA 3.0
:title-image: vshn.svg

// Other standard Asciidoctor attributes:
// https://asciidoctor.org/docs/user-manual/#builtin-attributes

// Reveal.js-specific attributes:
// https://asciidoctor.org/docs/asciidoctor-revealjs/#reveal-js-options

= Title

How we manage large numbers of ModSecurity WAF deployments, including CRS and custom rules

image::{title-image}[width=250]


== WAF as a Managed Service

TIP: Software you don't need to operate & maintain

[%step]
* Financial, insurance or legal sector
* Development platform (10-100+ apps)
* WAF a requirement for public-facing apps
* Custom rules for each application


== Scaling a Deployment of WAFs

TIP: How do we roll out & update 100+ WAFs?

[%step]
. Deployment configuration
. WAF + CRS update
. Update custom rules

Must scale! => Automation


== WAF as a Cloud Native App

TIP: Applications that are resilient & scale

[%step]
* Container image ➜ https://hub.docker.com/r/owasp/modsecurity-crs[`owasp/modsecurity-crs`]
* WAF parameters as environment variables
* Custom `rules.conf` mounted into container
* Kubernetes manifests (YAML)


== Environments & Automation

TIP: Development ➜ Integration ➜ Production

[%step]
* Automation with CI/CD (e.g. GitLab)
* Roll out the configuration only
* Automatic commits in Git repositories
* ModuleSync for many repos ➜ https://hub.docker.com/r/vshn/concierge/[`vshn/concierge`]


== Maintenance Workflow (1)

TIP: Make Changes to Git repositories

[%step]
* Add/remove repos ➜ `managed_modules.yml`
* Common content ➜ `moduleroot/`
* Custom changes ➜ `.sync.yml` (target repos)
* Create new WAF repo ➜ Cookiecutter


== Maintenance Workflow (2)

TIP: Working with feature branches

[%step]
. Feature branch ➜ deploys review app
. Merge into `master` ➜ deploys to Integration
. Push Git tag ➜ deploys to Production


[background-color="black"]
== Demo Time!

https://gitlab.com/vshn/waf-concierge[`gitlab.com/vshn/waf-concierge`]



== Thanks!

image::appuio.svg[]

{author} – {email}

[.small]
VSHN AG – Neugasse 10 – CH-8005 Zürich – +41 44 545 53 00 – https://vshn.ch – info@vshn.ch

link:slides.pdf[Download this presentation as a PDF file]

[.notes]
--
TODO: Ask your audience to download these slides!
--
